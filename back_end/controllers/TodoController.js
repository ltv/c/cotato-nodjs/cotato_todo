const todoModel = require('../models/todo')

exports.createTodo = (req, res) => {
  const todo = req.body  
  const todoInserted = todoModel.insert(todo)
  res.send(todoInserted)
}

exports.updateTodo = (req, res) => {
  const todo = req.body
  const todoUpdated = todoModel.updateById(todo)
  res.send(todoUpdated)
}

exports.getListTodo = (req, res) => {  
  const status  = parseInt(req.query.status, 10) || undefined
  const todos = todoModel.findAll(status)  
  res.send(todos)
}

exports.deleteTodo = (req, res) => {
  const {id} = req.body
  const result = todoModel.deleteById(id)
  res.send({result})
}