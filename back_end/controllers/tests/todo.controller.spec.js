const todoController = require('../TodoController')
const { mockRequest, mockResponse } = require('../../util/interceptor')

describe('test todo controller', () => {

  it('Should create todo and return todo inserted' ,async () => {
    const req = mockRequest();

    req.body = {
      id: 1,
      title: 'Cong viec 1'
    };
    const res = mockResponse();
    todoController.createTodo(req, res)
    expect.assertions(1)
    expect(res.send).toHaveBeenCalledWith({"id":1, "title": "Cong viec 1", "status": 0})
  })
  it('Should update todo and return todo updated' ,async () => {
    const req = mockRequest();

    req.body = {
      id: 1,
      title: 'Cong viec 2'
    };
    const res = mockResponse();
    todoController.updateTodo(req, res)
    expect.assertions(1)
    expect(res.send).toHaveBeenCalledWith({"id":1, "title": "Cong viec 2", "status": 0})
  })
  it('Should return list todo' ,async () => {
    const req = mockRequest();
    req.query.status = 1
    
    const res = mockResponse();    
    todoController.getListTodo(req, res)    
    expect.assertions(1)
    expect(res.send.mock.calls.length).toBe(1);
  })
  it('Should return true when delete success' ,async () => {
    const req = mockRequest();
    req.body.id = 1

    const res = mockResponse();    
    todoController.deleteTodo(req, res)    
    expect.assertions(1)
    expect(res.send).toHaveBeenCalledWith({"result": true})
  })
}) 