const express = require('express')
const router = express.Router()

const {createTodo, updateTodo, getListTodo, deleteTodo} = require('../controllers/TodoController')

router.post('/createTodo', createTodo)
router.post('/updateTodo', updateTodo)
router.get('/getListTodo', getListTodo)
router.post('/deleteTodo', deleteTodo)

module.exports = router