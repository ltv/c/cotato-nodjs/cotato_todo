const express = require('express')
const bodyParser = require('body-parser');
const allRoutes = require('express-list-endpoints');
const cors = require('cors')
const todoRouter = require('./routes/TodoRouter')

const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', todoRouter);

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => {
  console.log(`Todo app listening on port ${port}!`)
  console.log('Registered Routes: ');
  console.log(allRoutes(app));
})