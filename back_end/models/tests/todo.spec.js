const todoModel = require('../todo')

describe('test todo models', () => {
  it('Should create todo and return todo inserted' ,() => {
    const todoCreated = todoModel.insert({id: 1, title: 'cong viec 1'})
    expect.assertions(2)
    expect(todoCreated).toEqual({id:1, title: 'cong viec 1', status: 0})
    expect(todoModel.todos.length).toEqual(1)
  })

  it('Should update todo match with id and return todo updated', () => {
    const todoUpdated = todoModel.updateById({id: 1, title: 'update cv 1'})
    expect.assertions(2)
    expect(todoUpdated).toEqual({id: 1, title: 'update cv 1', status: 0})
    expect(todoModel.todos.length).toEqual(1)
  })

  it('should return false when not found todo by id', () => {
    const todoUpdated = todoModel.updateById({id: 2, title: 'update cv 1'})
    expect.assertions(1)
    expect(todoUpdated).toEqual(false)
  })

  it('should delete todo and return true', () => {
    const todo = todoModel.deleteById(1)
    expect.assertions(2)
    expect(todo).toEqual(true)
    expect(todoModel.todos.length).toEqual(0)
  })
  it('should return false', () => {
    todoModel.insert({id: 1, title: 'cong viec 1'})
    const todo = todoModel.deleteById(2)
    expect.assertions(2)
    expect(todo).toEqual(false)
    expect(todoModel.todos.length).toEqual(1)
  })

  it('should return all todos', () => {
    todoModel.insert({id: 2, title: 'cong viec 2'})
    const todos = todoModel.findAll()
    expect.assertions(1)
    expect(todoModel.todos).toEqual(todos)
  })
  it('should return todos has status = 1', () => {
    todoModel.insert({id: 3, title: 'cong viec 3'})
    todoModel.updateById({id:1 , status: 2})
    const todos = todoModel.findAll(0)    
    expect.assertions(1)
    expect(todos.length).toEqual(2)
  })
}) 