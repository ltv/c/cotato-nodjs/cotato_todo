const todos = []

/**
 * @todo - {id, title, status}
 * @status: 0 - created, 1 - inprogess, 2 - completed 
 */

/**
 * insert todo
 * @param todo - {id, title, status}
 * @return todo object
 */
exports.insert = (todo) => {
  const todoItem = {...todo, status: 0}
  todos.push(todoItem)
  return todoItem
}

/**
 * update todo by id
 * @param todo
 * @return todo updated || false
 */
exports.updateById = (todo) => {
  const todoIndex = todos.findIndex(t => t.id === todo.id)
  if (todoIndex !== -1) {
    todos[todoIndex] = {...todos[todoIndex], ...todo}
    return todos[todoIndex]
  } else {
    return false
  }
}

/**
 * delete todo by id
 * @param id
 * @return true: delete success || false
 */

exports.deleteById = (id) => {
  const todoIndex = todos.findIndex(item => item.id === id)
  if (todoIndex === -1){
    return false
  }
  todos.splice(todoIndex, 1)
  return true
}

exports.findAll = (status) => {    
  if (status === null || status === undefined) {
    return todos
  } else {
    return todos.filter(item => item.status === status)
  }
}

exports.todos = todos