import Service from '@/service';
import {Todo} from '@/store';
export class HomeService extends Service {
  public getTodoList(status?: number): Promise<Todo[]> {
    let query = {};
    if (status) {
      query = { status };
    }
    return this.get('getListTodo', query);
  }

  public createTodo(todo: Todo): Promise<Todo> {
    return this.post('createTodo', todo);
  }

  public updateTodo(todo: Todo): Promise<Todo> {
    return this.post('updateTodo', todo);
  }

  public deleteTodo(id: number): Promise<boolean> {
    return this.post('deleteTodo', { id });
  }
}
