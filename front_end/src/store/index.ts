import Vue from 'vue';
import Vuex from 'vuex';

import {HomeService} from '../views/Home/HomeService';
const homeService = new HomeService();
Vue.use(Vuex);

export interface Todo {
  id: number;
  status?: number;
  title: string;
}

interface RootState {
  todos: Todo[];
  visibility: 'all' | 'active' | 'completed';
}

export default new Vuex.Store<RootState>({
  state: {
    todos: [],
    visibility: 'all',
  },
  mutations: {
    ADD_NEW_TODO(state, todo) {
      const { todos } = state;
      todos.push(todo);
      state.todos = [...todos];
    },
    SET_ALL_DONE(state) {
      state.todos.forEach((todo: Todo) => {
        todo.status = 3;
      });
    },
    REMOVE_TODO(state, todo) {
      state.todos.splice(state.todos.indexOf(todo), 1);
    },
    REMOVE_COMPLETED(state) {
      state.todos = state.todos.filter((todo: any) => !todo.completed);
    },
    UPDATE_TODO(state, todo: Todo) {
      const { todos } = state;
      const todoIdx: number = todos.findIndex((t: Todo) => t.id === todo.id);
      if (todoIdx >= 0) {
        todos[todoIdx] = { ...todo };
      }
    },
    SET_TODO_LIST(state, todos: Todo[]) {
      state.todos = [...todos];
    },
  },
  actions: {
    async addNewTodo({ commit, state }, title: string) {
      const { todos } = state;
      const todo = await homeService.createTodo({
        id: todos.length + 1,
        title,
      });
      if (todo) {
        commit('ADD_NEW_TODO', todo);
      }
    },
    setAllDone({ commit }) {
      commit('SET_ALL_DONE');
    },
    async removeTodo({ commit }, todo: Todo) {
      const todoDeleted = await homeService.deleteTodo(todo.id);
      if (todoDeleted) {
        commit('REMOVE_TODO', todo);
      }
    },
    removeCompleted({ commit }) {
      commit('REMOVE_COMPLETED');
    },
    async updateTodo({ commit }, todo: Todo) {
      const todoUpdated = await homeService.updateTodo(todo);
      if (todoUpdated) {
        commit('UPDATE_TODO', todoUpdated);
      }
    },
    async getTodoList({ commit }, status?: number) {
      const todos = await homeService.getTodoList();
      commit('SET_TODO_LIST', todos);
    },
  },
  getters: {
    all(state) {
      return state.todos;
    },
    active(state) {
      return state.todos.filter((todo: any) => !todo.completed);
    },
    completed(state) {
      return state.todos.filter((todo: any) => todo.completed);
    },
  },
  modules: {},
});
